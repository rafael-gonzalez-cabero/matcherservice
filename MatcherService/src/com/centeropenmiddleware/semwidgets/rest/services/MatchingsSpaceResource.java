package com.centeropenmiddleware.semwidgets.rest.services;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.centeropenmiddleware.semwidgets.matcher.FriendCodeMatcher;
import com.centeropenmiddleware.semwidgets.matcher.Matcher;
import com.centeropenmiddleware.semwidgets.matcher.SemanticMatcher;
import com.centeropenmiddleware.semwidgets.model.MatchingsSpace;
import com.centeropenmiddleware.semwidgets.model.Update;


@Path("/matcher")
public class MatchingsSpaceResource {
	public static final String MATCHER_ATTRIBUTE = "MATCHER";

	@Context
	ServletContext context;

	// --------------------------------------------------------------------------------

	private Matcher _getMatcher() {
		Matcher matcher = (Matcher) this.context
				.getAttribute(MatchingsSpaceResource.MATCHER_ATTRIBUTE);
		if (matcher == null) {
			matcher = new FriendCodeMatcher();
			this.context.setAttribute(MatchingsSpaceResource.MATCHER_ATTRIBUTE,
					matcher);
		}
		return matcher;
	}

	// --------------------------------------------------------------------------------

	@GET
	@Path("/matchingsSpace/{ID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMatchingsSpaceInJSON(
			@DefaultValue("none") @PathParam("ID") String ID) {
		System.out.println("GET: matcher/matchingsSpace/" + ID);
		Matcher matcher = this._getMatcher();
		MatchingsSpace matchingsSpace = matcher.getMatchingSpace(ID);
		if (matchingsSpace != null) {
			return Response.ok(matchingsSpace, MediaType.APPLICATION_JSON)
					.build();
		} else {
			return Response.noContent().build();
		}
	}

	// --------------------------------------------------------------------------------

	@GET
	@Path("/matchingsSpace/{ID}/lastUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdateJSON(
			@DefaultValue("none") @PathParam("ID") String ID) {
		System.out.println("GET: matcher/matchingsSpace/lastUpdate");
		Update update = this._getMatcher().getLastUpdate(ID);
		if (update != null) {
			return Response.ok(update, MediaType.APPLICATION_JSON).build();
		} else {
			return Response.noContent().build();
		}

	}

	// --------------------------------------------------------------------------------

	@PUT
	@Path("/matchingsSpace/{ID}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createMatchingSpaceInJSON(
			@DefaultValue("none") @PathParam("ID") String ID) {
		System.out.println("PUT: matcher/matchingsSpace/" + ID);
		this._getMatcher().createMatchingsSpace(ID);
		// this._getMatcher().updateMatchingSpace(update);

		return Response.ok().build();
	}

	// --------------------------------------------------------------------------------

	@POST
	@Path("/matchingsSpace/{ID}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateMatchingSpaceInJSON(
			@DefaultValue("none") @PathParam("ID") String ID, Update update) {
		System.out.println("POST: matcher/matchingsSpace");
		System.out.println("update: " + update);
		this._getMatcher().updateMatchingSpace(update, ID);

		return Response.ok().build();
	}

	// --------------------------------------------------------------------------------

	@DELETE
	@Path("/matchingsSpace/{ID}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response init(@DefaultValue("none") @PathParam("ID") String ID) {
		System.out.println("DELETE: matcher/matchingsSpace/" + ID);
		System.out.println("Initializing the matcher service");
		this._getMatcher().deleteMatchingsSpace(ID);
		return Response.ok().build();
	}

}
