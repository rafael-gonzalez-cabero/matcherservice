package com.centeropenmiddleware.semwidgets.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class MatchingsSpace {
	private List<Matching> matchings;
	private String timestamp;
	private String ID;

	public MatchingsSpace() {
		this.matchings = new ArrayList<Matching>();
	}

	public MatchingsSpace(String ID) {
		this.matchings = new ArrayList<Matching>();
		this.ID = ID;
		java.util.Date date = new java.util.Date();
		Timestamp timeStamp = new Timestamp(date.getTime());
		this.timestamp = timeStamp.toString();
	}

	public List<Matching> getMatchings() {
		return matchings;
	}

	public void setMatchings(List<Matching> matchings) {
		this.matchings = matchings;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return this.matchings.toString();
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		this.ID = iD;
	}

}
