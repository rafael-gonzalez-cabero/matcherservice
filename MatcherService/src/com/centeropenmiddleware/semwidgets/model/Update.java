package com.centeropenmiddleware.semwidgets.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Update {
	public static final String TYPE_ADDITION = "addition";
	public static final String TYPE_REMOVAL = "removal";

	
	private List<FieldDescription> fields;
	private String type;
	private List<String> ontologies;

	//---------------------------------------------------------------------
	
	public Update() {
		this.fields = new ArrayList<FieldDescription>();

		this.ontologies = new ArrayList<String>();
	}
	
	//---------------------------------------------------------------------

	public List<String> getOntologies() {
		return ontologies;
	}
	
	//---------------------------------------------------------------------

	public void setOntologies(List<String> ontologies) {
		this.ontologies = ontologies;
	}

	//---------------------------------------------------------------------
	
	@Override
	public String toString() {
		return "Update[fields:" + this.fields + ", updateType:" + this.type
				+ ", ontologies:" + this.ontologies + "]";
	}
	
	//---------------------------------------------------------------------

	public List<FieldDescription> getFields() {
		return fields;
	}
	
	//---------------------------------------------------------------------

	public void setFields(List<FieldDescription> fields) {
		this.fields = fields;
	}
	
	//---------------------------------------------------------------------

	public String getType() {
		return type;
	}

	//---------------------------------------------------------------------
	
	public void setType(String type) {
		this.type = type;

	}
}
