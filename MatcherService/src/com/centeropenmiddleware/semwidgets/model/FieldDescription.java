package com.centeropenmiddleware.semwidgets.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FieldDescription {
	public static final String FLOW_INPUT = "input";
	public static final String FLOW_OUTPUT = "output";
	private String ID;
	private String semanticType;
	private String syntacticType;
	private String flow;
	
//--------------------
	public FieldDescription(){
		
	}
	public FieldDescription(String ID, String semanticType, String syntacticType, String flow){
		this.ID = ID;
		this.semanticType = semanticType;
		this.syntacticType = syntacticType;
		this.flow = flow;
	}
	
	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getSemanticType() {
		return semanticType;
	}

	public void setSemanticType(String semanticType) {
		this.semanticType = semanticType;
	}

	public String getSyntacticType() {
		return syntacticType;
	}

	public void setSyntacticType(String syntacticType) {
		this.syntacticType = syntacticType;
	}
	
	public String getFlow() {
		return flow;
	}
	public void setFlow(String flow) {
		this.flow = flow;
	}

	@Override
	public String toString(){
		return "FieldDescription [ID:"+this.ID+", semanticType:"+this.semanticType+", syntacticType:"+this.syntacticType+", flow:"+this.flow+"]";
	}
	
	@Override
	public boolean equals(Object object){
		if (object instanceof FieldDescription){
			FieldDescription other = (FieldDescription)object;
			return (this.ID.equals(other.ID));
		}else{
			return false;
		}
	}
}
