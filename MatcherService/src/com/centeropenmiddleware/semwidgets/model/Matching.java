package com.centeropenmiddleware.semwidgets.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Matching {
	public static String MATCH_CODE_PLUGIN = "PLUGIN";
	public static String MATCH_CODE_EQUIVALENT = "EQUIVALENT";
	public static String MATCH_CODE_SUBSUMES = "SUBSUMES";
	public static String MATCH_CODE_SUBSUMED = "SUBSUMED";
	public static String MATCH_CODE_OVERLAPS = "OVERLAPS";
	public static String MATCH_CODE_DISJOINT = "DISJOINT";
	public static String MATCH_CODE_NONE = "NONE";

	private String origin;
	private String destination;

	private String matchCode;

	// -----------------------------------------------------------------------
	public String getOrigin() {
		return origin;
	}

	// -----------------------------------------------------------------------

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	// -----------------------------------------------------------------------

	public String getDestination() {
		return destination;
	}

	// -----------------------------------------------------------------------

	public void setDestination(String destination) {
		this.destination = destination;
	}

	// -----------------------------------------------------------------------

	public String getMatchCode() {
		return matchCode;
	}

	// -----------------------------------------------------------------------

	public void setMatchCode(String matchCode) {
		this.matchCode = matchCode;
	}

	// -----------------------------------------------------------------------

	@Override
	public String toString() {
		return "Mapping[ origin:" + this.origin + ", destination: "
				+ this.destination + ", matchCode:" + matchCode + "]";
	}

	// -----------------------------------------------------------------------

	@Override
	public boolean equals(Object other) {
		if (other instanceof Matching) {
			return (this.destination
					.equals(((Matching) other).getDestination()) && this.origin
					.equals(((Matching) other).getOrigin()));

		}
		return false;
	}
}
