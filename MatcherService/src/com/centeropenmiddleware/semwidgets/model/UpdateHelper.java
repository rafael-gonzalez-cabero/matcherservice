package com.centeropenmiddleware.semwidgets.model;


public class UpdateHelper {

	public static Update _generateUpdate() {
		Update update = new Update();

		// Oldfields
		String[] lettersOldFields = { "C", "D" };
		FieldDescription newFieldDescription;

		for (int i = 0; i < lettersOldFields.length; i++) {
			if ((i % 2) == 0) {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersOldFields[i], "whoever", "type"
						+ lettersOldFields[i], FieldDescription.FLOW_OUTPUT);
			} else {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersOldFields[i], "whatever","type" + lettersOldFields[i],
						FieldDescription.FLOW_INPUT);
			}

			update.getFields().add(newFieldDescription);
			update.setType(Update.TYPE_ADDITION);
		}

		return update;
	}
	public static Update _generateSecondUpdate() {
		Update update = new Update();

		// Newfields
		String[] lettersNewFields = { "A", "B" };

		FieldDescription newFieldDescription;
		for (int i = 0; i < lettersNewFields.length; i++) {
			if ((i % 2) == 0) {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersNewFields[i], "whatever", "type" + lettersNewFields[i],
						FieldDescription.FLOW_OUTPUT);
			} else {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersNewFields[i], "whoever",  "type"
						+ lettersNewFields[i], FieldDescription.FLOW_INPUT);
			}

			update.getFields().add(newFieldDescription);
			update.setType(Update.TYPE_ADDITION);
		}


		return update;
	}
	
	public static Update _generateAnnotatedUpdate() {
		Update update = new Update();

		// Oldfields
		String[] lettersOldFields = { "C", "D" };
		FieldDescription newFieldDescription;

		for (int i = 0; i < lettersOldFields.length; i++) {
			if ((i % 2) == 0) {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersOldFields[i], "Compra", "type"
						+ lettersOldFields[i], FieldDescription.FLOW_OUTPUT);
			} else {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersOldFields[i], "Pago","type" + lettersOldFields[i],
						FieldDescription.FLOW_INPUT);
			}

			update.getFields().add(newFieldDescription);
			update.setType(Update.TYPE_ADDITION);
		}

		return update;
	}
	
	
	
	public static Update _generateSecondAnnotatedUpdate() {
		Update update = new Update();

		// Newfields
		String[] lettersNewFields = { "A", "B" };

		FieldDescription newFieldDescription;
		for (int i = 0; i < lettersNewFields.length; i++) {
			if ((i % 2) == 0) {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersNewFields[i], "<http://www.centeropenmiddleware.com/ontology/flightsDemo#Pago>", "type" + lettersNewFields[i],
						FieldDescription.FLOW_OUTPUT);
			} else {
				newFieldDescription = new FieldDescription("vendor/name/version/field"
						+ lettersNewFields[i], "CompraEnDolares",  "type"
						+ lettersNewFields[i], FieldDescription.FLOW_INPUT);
			}

			update.getFields().add(newFieldDescription);
			update.setType(Update.TYPE_ADDITION);
		}


		return update;
	}
}
