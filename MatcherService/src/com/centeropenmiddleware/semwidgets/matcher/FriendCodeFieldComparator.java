package com.centeropenmiddleware.semwidgets.matcher;

import com.centeropenmiddleware.semwidgets.model.Matching;

public class FriendCodeFieldComparator implements FieldComparator {

	@Override
	public String compare(String originField, String destinationField) {

		System.out.println("_fieldBasedValuateMatching " + originField + ", "
				+ destinationField);

		if (originField.equals(destinationField)) {
			return Matching.MATCH_CODE_EQUIVALENT;
		}
		return Matching.MATCH_CODE_NONE;
	}

}
