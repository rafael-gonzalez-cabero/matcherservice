package com.centeropenmiddleware.semwidgets.matcher;

public interface FieldComparator {
	public String compare(String originField, String destinationField);
}
