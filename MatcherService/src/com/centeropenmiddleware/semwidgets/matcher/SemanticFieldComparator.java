package com.centeropenmiddleware.semwidgets.matcher;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

import com.centeropenmiddleware.semwidgets.model.Matching;

public class SemanticFieldComparator implements FieldComparator {

	private RelationChecker relationChecker;

	public SemanticFieldComparator() {

		try {
			this.relationChecker = new MultiOntologyRelationChecker();
			((MultiOntologyRelationChecker) relationChecker)
					.addOntology(SemanticMatcher.class
							.getResource("flights.owl"));
		} 
		catch (OWLOntologyCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String compare(String originField, String destinationField) {

		System.out.println("_semanticevaluateMatching " + originField + ", "
				+ destinationField);
		new DefaultPrefixManager();
		SimpleShortFormProvider simpleShortFormProvider = new SimpleShortFormProvider();
		OWLClass originFieldClass = null;

		OWLClass destinationFieldClass = null;

		try {

			originFieldClass = this.relationChecker.getCandidate(originField)
					.asOWLClass();
			destinationFieldClass = this.relationChecker.getCandidate(destinationField)
					.asOWLClass();
			
		} catch (ParserException e1) {
			
			//e1.printStackTrace();
			
		}
		if (originFieldClass==null||destinationFieldClass==null){
			return Matching.MATCH_CODE_NONE;
		}
		
		
		
		try {
			if(relationChecker.disjointConcepts(originField).contains(destinationFieldClass)){
				return Matching.MATCH_CODE_DISJOINT;
			}
		
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(relationChecker.equivalentConcepts(originField).contains(destinationFieldClass)){
				return Matching.MATCH_CODE_EQUIVALENT;
			}
		
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			if(relationChecker.subsumedConcepts(originField).contains(destinationFieldClass)){
				return Matching.MATCH_CODE_SUBSUMES;
			}
		
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	/*
		try {
			for (OWLClass c : relationChecker.subsumedConcepts(originField))
				System.out.println("short> "
						+ simpleShortFormProvider.getShortForm(c) + "noemal> "
						+ c);
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}
		

	*/	
		try {
			if(relationChecker.subsumingConcepts(originField).contains(destinationFieldClass)){
				return Matching.MATCH_CODE_SUBSUMED;
			}
		
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(relationChecker.disjointConcepts(originField).contains(destinationFieldClass)){
				return Matching.MATCH_CODE_DISJOINT;
			}
		
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return Matching.MATCH_CODE_NONE;
	}

	public RelationChecker getRelationChecker() {
		return relationChecker;
	}

	public void setRelationChecker(RelationChecker relationChecker) {
		this.relationChecker = relationChecker;
	}
}
