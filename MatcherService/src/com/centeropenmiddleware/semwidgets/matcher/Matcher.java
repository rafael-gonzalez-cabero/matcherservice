package com.centeropenmiddleware.semwidgets.matcher;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.centeropenmiddleware.semwidgets.model.FieldDescription;
import com.centeropenmiddleware.semwidgets.model.Matching;
import com.centeropenmiddleware.semwidgets.model.MatchingsSpace;
import com.centeropenmiddleware.semwidgets.model.Update;

public abstract class Matcher {
	private Map<String, MatchingsSpace> matchingSpaces;
	private Map<String, Map<String, FieldDescription>> inputFields;
	private Map<String, Map<String, FieldDescription>> outputFields;
	private Map<String, Update> lastUpdate;
	protected FieldComparator fieldComparator;

	// --------------------------------------------------------------------------------

	public Matcher() {
		this.matchingSpaces = new HashMap<String, MatchingsSpace>();
		this.inputFields = new HashMap<String, Map<String, FieldDescription>>();
		this.outputFields = new HashMap<String, Map<String, FieldDescription>>();
		this.lastUpdate = new HashMap<String, Update>();
	}

	// --------------------------------------------------------------------------------

	public void init() {
		this.inputFields = new HashMap<String, Map<String, FieldDescription>>();
		this.outputFields = new HashMap<String, Map<String, FieldDescription>>();
		this.matchingSpaces = new HashMap<String, MatchingsSpace>();
		this.lastUpdate = new HashMap<String, Update>();
	}

	// --------------------------------------------------------------------------------

	public void deleteMatchingsSpace(String ID) {
		this.inputFields.remove(ID);
		this.outputFields.remove(ID);
		this.matchingSpaces.remove(ID);
		this.lastUpdate.remove(ID);
	}

	// --------------------------------------------------------------------------------

	public void createMatchingsSpace(String ID) {
		this.inputFields.put(ID, new HashMap<String, FieldDescription>());
		this.outputFields.put(ID, new HashMap<String, FieldDescription>());
		this.matchingSpaces.put(ID, new MatchingsSpace(ID));

	}

	// --------------------------------------------------------------------------------

	public void updateMatchingSpace(Update update, String ID) {
		this.lastUpdate.put(ID, update);
		System.out
				.println("Fields description---------------------------------------------------------------------");
		for (FieldDescription fieldDescription : update.getFields()) {
			System.out.println(fieldDescription);

		}
		System.out.println("Type:" + update.getType());

		if (Update.TYPE_ADDITION.equals(update.getType())) {
			_addWidget(update, ID);
		} else {
			_removeWidget(update, ID);
		}
		java.util.Date date = new java.util.Date();
		Timestamp timeStamp = new Timestamp(date.getTime());
		System.out.println("ts------------> " + timeStamp);
		this.matchingSpaces.get(ID).setTimestamp(timeStamp.toString());
	}

	// --------------------------------------------------------------------------------

	public MatchingsSpace getMatchingSpace(String ID) {

		return this.matchingSpaces.get(ID);
	}

	// --------------------------------------------------------------------------------

	private void _addWidget(Update update, String ID) {
		System.out.println("_addWidget " + update);

		_findMatchingNewInputFields(update.getFields(), ID);
		_findMatchingNewOutputFields(update.getFields(), ID);
		_updateFields(update.getFields(), ID);
	}

	// --------------------------------------------------------------------------------

	private void _updateFields(List<FieldDescription> newFields, String ID) {
		for (FieldDescription field : newFields) {
			if (FieldDescription.FLOW_INPUT.equals(field.getFlow())) {
				this.inputFields.get(ID).put(field.getID(), field);
			} else {
				this.outputFields.get(ID).put(field.getID(), field);
			}
		}
	}

	// --------------------------------------------------------------------------------

	private void _findMatchingNewInputFields(List<FieldDescription> newFields,
			String ID) {
		System.out.println("_findMatchingNewInputFields " + newFields);
		for (FieldDescription fieldDescription : newFields) {
			if (FieldDescription.FLOW_INPUT.equals(fieldDescription.getFlow())) {
				_generateOutputFieldsCandidates(fieldDescription, ID);
			}
		}
	}

	// --------------------------------------------------------------------------------

	private void _findMatchingNewOutputFields(List<FieldDescription> newFields,
			String ID) {
		System.out.println("_findMatchingNewOutputFields " + newFields);
		for (FieldDescription fieldDescription : newFields) {
			if (FieldDescription.FLOW_OUTPUT.equals(fieldDescription.getFlow())) {
				_generateInputFieldsCandidates(fieldDescription, ID);
			}
		}
	}

	// --------------------------------------------------------------------------------

	private void _generateOutputFieldsCandidates(FieldDescription inputField,
			String ID) {
		System.out.println("_generateOutputFieldsCandidates "
				+ inputField.getSemanticType());
		for (FieldDescription outputFields : this.outputFields.get(ID).values()) {
			String result = this.fieldComparator.compare(
					outputFields.getSemanticType(),
					inputField.getSemanticType());
			System.out.println(">>" + result);
			if (!result.equals(Matching.MATCH_CODE_NONE)) {

				Matching matching = new Matching();
				matching.setDestination(inputField.getID());
				matching.setOrigin(outputFields.getID());
				matching.setMatchCode(result);
				if (!this.matchingSpaces.get(ID).getMatchings()
						.contains(matching)) {
					this.matchingSpaces.get(ID).getMatchings().add(matching);
				} else {
					System.out.println("repetidos!");
				}

			}
		}
		// System.out.println(" Matchings: " + this.matchingSpace);
	}

	// --------------------------------------------------------------------------------

	private void _generateInputFieldsCandidates(FieldDescription outputField,
			String ID) {
		System.out.println("_generateOutputFieldsCandidates "
				+ outputField.getSemanticType());
		for (FieldDescription inputFields : this.inputFields.get(ID).values()) {
			String result = this.fieldComparator.compare(
					outputField.getSemanticType(),
					inputFields.getSemanticType());
			System.out.println(">>" + result);
			if (!result.equals(Matching.MATCH_CODE_NONE)) {
				Matching matching = new Matching();
				matching.setOrigin(outputField.getID());
				matching.setDestination(inputFields.getID());
				matching.setMatchCode(result);
				if (!this.matchingSpaces.get(ID).getMatchings()
						.contains(matching)) {
					this.matchingSpaces.get(ID).getMatchings().add(matching);
				} else {
					System.out.println("repetidos!");
				}
			}
		}
		// System.out.println(" Matchings: " + this.matchingSpace);
	}

	// --------------------------------------------------------------------------------

	private String _evaluateMatch(String originSemanticType,
			String destinationSemanticType) {
		System.out.println("_evaluateMatching " + originSemanticType + ", "
				+ destinationSemanticType);

		if (originSemanticType.equals(destinationSemanticType)) {
			return Matching.MATCH_CODE_EQUIVALENT;
		}
		return Matching.MATCH_CODE_NONE;
	}

	// --------------------------------------------------------------------------------

	private void _removeWidget(Update update, String ID) {
		System.out.println("_removeWidget " + update + " ,ID " + ID);

		for (FieldDescription field : update.getFields()) {
			_removeUpdateField(field, ID);

		}
		for (FieldDescription field : update.getFields()) {
			_removeRelatedMatchings(field, ID);
		}

	}

	private void _removeUpdateField(FieldDescription field, String ID) {
		if (FieldDescription.FLOW_OUTPUT.equals(field.getFlow())) {
			this.outputFields.get(ID).remove(field.getID());
		} else {
			this.inputFields.get(ID).remove(field.getID());
		}

	}

	// ----------------------------------------------------------------------------------
	/**
	 * This method removes all the matching that either has field as an origin
	 * or a destination
	 * 
	 * @param field
	 *            The field to be removed from the matchings space
	 * @param ID
	 *            The ID of the matchings space to be considered
	 */

	private void _removeRelatedMatchings(FieldDescription field, String ID) {
		ArrayList<Matching> matchingsToRemove = new ArrayList<Matching>();
		for (Matching matching : this.matchingSpaces.get(ID).getMatchings()) {

			if ((matching.getOrigin().equals(field.getID()))
					|| (matching.getDestination().equals(field.getID()))) {
				matchingsToRemove.add(matching);
			}
		}
		for (Matching matching : matchingsToRemove) {
			this.matchingSpaces.get(ID).getMatchings().remove(matching);
		}
	}

	// --------------------------------------------------------------------------------

	public Update getLastUpdate(String ID) {
		return lastUpdate.get(ID);
	}

	// --------------------------------------------------------------------------------

	public void setLastUpdate(Update lastUpdate, String ID) {
		this.lastUpdate.put(ID, lastUpdate);
	}

	// --------------------------------------------------------------------------------

	public FieldComparator getFieldComparator() {
		return fieldComparator;
	}

	// --------------------------------------------------------------------------------

	public void setFieldComparator(FieldComparator fieldComparator) {
		this.fieldComparator = fieldComparator;
	}
}
