package com.centeropenmiddleware.semwidgets.matcher;

public class SemanticMatcher extends Matcher {

	// --------------------------------------------------------------------------------

	public SemanticMatcher() {

		fieldComparator = new SemanticFieldComparator();
	}

	public static void main(String[] args) {
		SemanticMatcher semanticMatcher = new SemanticMatcher();
		System.out.println(semanticMatcher.getFieldComparator().compare("Compra", "CompraEnDolares"));
	}

}
