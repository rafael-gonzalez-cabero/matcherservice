package com.centeropenmiddleware.semwidgets.matcher;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

/**
 * Hello world!
 * 
 */
public class Main {
	public static void main(String[] args) throws OWLOntologyCreationException,
			MalformedURLException, IOException, URISyntaxException {
		System.out.println("I'm going to do some things, don't mind me.");
		MultiOntologyRelationChecker relationChecker = new MultiOntologyRelationChecker();
		relationChecker.addOntology(new File("/testNames.owl").toURI()
				.toURL());
		new DefaultPrefixManager();
		SimpleShortFormProvider simpleShortFormProvider = new SimpleShortFormProvider();
		OWLClass classE = null;
		try {
			classE = relationChecker.getCandidate("E").asOWLClass();

		} catch (ParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if (relationChecker.subsumedConcepts("A").contains(classE)){
				System.out.println("ESTA!!!!");
			}
		} catch (ParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		System.out.println("\nSubsumed!");
		try {
			for (OWLClass c : relationChecker.subsumedConcepts("A"))
				System.out.println("short> "
						+ simpleShortFormProvider.getShortForm(c) + "noemal> "
						+ c);
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nSubsuming!");
		try {
			for (OWLClass c : relationChecker.subsumingConcepts("C"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nEquivalent!");
		try {
			for (OWLClass c : relationChecker.equivalentConcepts("E"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nDisjoint!");
		try {
			for (OWLClass c : relationChecker.disjointConcepts("D"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		relationChecker.addOntology(new File("resources/testRelations.owl")
				.toURI().toURL());

		System.out.println("\nSubsumed!");
		try {
			for (OWLClass c : relationChecker.subsumedConcepts("A"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nSubsuming!");
		try {
			for (OWLClass c : relationChecker.subsumingConcepts("D"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nEquivalent!");
		try {
			for (OWLClass c : relationChecker.equivalentConcepts("F"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nDisjoint!");
		try {
			for (OWLClass c : relationChecker.disjointConcepts("E"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		relationChecker.addOntology(new File("resources/testCollisions.owl")
				.toURI().toURL());

		System.out.println("\nSubsumed!");
		try {
			for (OWLClass c : relationChecker.subsumedConcepts("A"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nSubsuming!");
		try {
			for (OWLClass c : relationChecker.subsumingConcepts("H"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nEquivalent!");
		try {
			for (OWLClass c : relationChecker.equivalentConcepts("B"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nDisjoint!");
		try {
			for (OWLClass c : relationChecker
					.disjointConcepts("<http://www.centeropenmiddleware.com/ontology/tests/jmora/testNames#B>"))
				System.out.println(simpleShortFormProvider.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

	}
}
