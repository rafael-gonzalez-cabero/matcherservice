package com.centeropenmiddleware.semwidgets.matcher;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;


/**
 * Hello world!
 * 
 */
public class MerguedMain {
	public static void main(String[] args) throws OWLOntologyCreationException, MalformedURLException, IOException, URISyntaxException {
		System.out.println("I'm going to do some things, don't mind me.");
		MultiOntologyRelationChecker relationChecker = new MultiOntologyRelationChecker();
		relationChecker.addOntology(new File("resources/testNames.owl").toURI().toURL());
		relationChecker.addOntology(new File("resources/bank.owl").toURI().toURL());
		new DefaultPrefixManager();
		SimpleShortFormProvider ssfp = new SimpleShortFormProvider();

		System.out.println("\nClasses");
		try {
			for (OWLClass c : relationChecker.getClasses())
				System.out.println(ssfp.getShortForm(c));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		
		System.out.println("\nSubsumed!");
		try {
			for (OWLClass c : relationChecker.subsumedConcepts("A"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nSubsuming!");
		try {
			for (OWLClass c : relationChecker.subsumingConcepts("C"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nEquivalent!");
		try {
			for (OWLClass c : relationChecker.equivalentConcepts("E"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nDisjoint!");
		try {
			for (OWLClass c : relationChecker.disjointConcepts("D"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		relationChecker.addOntology(new File("resources/testRelations.owl").toURI().toURL());

		System.out.println("\nSubsumed!");
		try {
			for (OWLClass c : relationChecker.subsumedConcepts("A"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nSubsuming!");
		try {
			for (OWLClass c : relationChecker.subsumingConcepts("D"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nEquivalent!");
		try {
			for (OWLClass c : relationChecker.equivalentConcepts("F"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nDisjoint!");
		try {
			for (OWLClass c : relationChecker.disjointConcepts("E"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		relationChecker.addOntology(new File("resources/testCollisions.owl").toURI().toURL());

		System.out.println("\nSubsumed!");
		try {
			for (OWLClass c : relationChecker.subsumedConcepts("A"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nSubsuming!");
		try {
			for (OWLClass c : relationChecker.subsumingConcepts("H"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nEquivalent!");
		try {
			for (OWLClass c : relationChecker.equivalentConcepts("B"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("\nDisjoint!");
		try {
			for (OWLClass c : relationChecker.disjointConcepts("<http://www.centeropenmiddleware.com/ontology/tests/jmora/testNames#B>"))
				System.out.println(ssfp.getShortForm(c));
		} catch (ParserException e) {
			System.err.println(e.getMessage());
		}

	}
}
