package com.centeropenmiddleware;


import com.centeropenmiddleware.semwidgets.model.MatchingsSpace;
import com.centeropenmiddleware.semwidgets.model.Update;
import com.centeropenmiddleware.semwidgets.model.UpdateHelper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class LocalSemanticMatcherServiceClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String baseURL = "http://localhost:8080/MatcherService/rest";

		ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		Client client = Client.create(config);

		WebResource service = client.resource(baseURL);

		ClientResponse response = null;
		// Create one todo

		Update update = UpdateHelper._generateAnnotatedUpdate();

		Update secondUpdate = UpdateHelper._generateSecondAnnotatedUpdate();
		

		/*
		 * WebResource webResource = client
		 * .resource("http://localhost:8080/matcher");
		 */
		String ID = "whatever";
		System.out.println("Creating the matchings space  " + ID);
		
		System.out.println(service.path("/semanticmatcher/matchingsSpace/"+ID).type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		.put(ClientResponse.class));
		
		

		// webResource.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		// .post(Track.class, newTrack);
		System.out.println("First update: " + update);
		service.path("/semanticmatcher/matchingsSpace/"+ID)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, update);
		System.out.println("--");
	
		System.out.println("Second update: " + secondUpdate);
		service.path("/semanticmatcher/matchingsSpace/"+ID)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, secondUpdate);

		// System.out.println(response.getEntity(String.class));
		/*
		 * service.type(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(
		 * ClientResponse.class, secondUpdate);
		 */
		/*
		System.out.println("Second update (repeated): " + secondUpdate);
		service.path("/semanticmatcher/matchingsSpace/"+ID)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, secondUpdate);
		
		*/
		
		System.out.println("Result(toString) "
				+ service.path("/semanticmatcher/matchingsSpace/"+ID)
						.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON)
						.get(ClientResponse.class)
						.getEntity(MatchingsSpace.class));
		
		secondUpdate.setType(Update.TYPE_REMOVAL);
		System.out.println("Removing the second update: " + secondUpdate);
		service.path("/semanticmatcher/matchingsSpace/"+ID)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, secondUpdate);
		
		secondUpdate.setType(Update.TYPE_ADDITION);
		System.out.println("Reintroducing the second update: " + secondUpdate);
		service.path("/semanticmatcher/matchingsSpace/"+ID)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, secondUpdate);
		
		System.out.println("The matchings space ");
		System.out.println("Result(toString) "
				+ service.path("/semanticmatcher/matchingsSpace/"+ID)
						.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON)
						.get(ClientResponse.class)
						.getEntity(MatchingsSpace.class));
		
		
		
		
		service.path("/semanticmatcher/matchingsSpace/"+ID)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.delete(ClientResponse.class);

		System.out.println("Trying to get matchingsSpace that has been deleted");
		System.out.println(
				service.path("/semanticmatcher/matchingsSpace/"+ID)
						.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON)
						.get(ClientResponse.class)
						.getEntity(MatchingsSpace.class));
		
	
		System.out
				.println("exit-------------------------------------------------------");

		System.exit(0);

	}

	

	

}
